# REEKiLib

This is a parts library for [KiCAD][kicad_link] from [RealEE][realee_link] containing schematic symbols, PCB footprints, 3D models, simulation model, templates and others utils for HW design.  
This [repo][repo_link] is not intended to be added as a git submodule as it is quite heavy, rather you should install it locally on your workstation.

## Compatibility

This library is mainly aimed at the [stable][kicad_install_link] version (currently 8) but may be compatible with [previous][kicad_install_link] (6,7) and [newer][kicad_install_link] (9+) versions.

## Release

See the [roadmap file][roadmap_file] for upcoming features or see the [release file][release_file] for currently releases version

## Organization

- _[3dmodel][3dmodel_folder]_: 3D models used in the footprints
- _[doc][doc_folder]_: Library documentation, convention, processes and other information
- _[footprint][footprint_folder]_: Standard and custom PCB footprints grouped by type inside subfolder
- _[simmodel][simmodel_folder]_: Simulation (SPICE & IBIS) parts model _(limited to publicly available models)_
- _[symbol][symbol_folder]_: Schematic symbol available in this lib

## Install

### Library

1. clone the [repo][repo_link] to a local folder accessible to KiCAD
2. create an [environment variable][kicad_doc_path_link] inside KiCAD named `REEKILIB` pointing to the root folder of the repo
3. [install][kicad_doc_libinstall_link] the necessary symbol lib from the folder [symbol][symbol_folder]
4. [install][kicad_doc_libinstall_link] the necessary footprint lib from the folder [footprint][footprint_folder]

### Tools

1. Ensure you have [python][python_install_link], [pyenv][pyenv_install_link] & [poetry][poetry_install_link] installed
2. Install the venv: `poetry install`
3. Invoke the tools: `poe command` _command_ being one of those possible:
   - `check-groups` verify that all parts group name in documentation are unique

## Contribution

[Contribution][contrib_file] guideline is not done yet, but old ones are ported

## License

[GPL-v3.0][license_file] or [contact us][maintainer_email]

<!-- links -->

[realee_link]: https://www.realee.tech/
[repo_link]: https://gitlab.com/real-ee/public/reekilib
[kicad_link]: https://www.kicad.org/
[freecad_link]: https://www.freecadweb.org/

<!-- install links -->

[kicad_install_link]: https://www.kicad.org/download/
[python_install_link]: https://laurencedv.org/computing/python#python
[pyenv_install_link]: https://laurencedv.org/computing/python#pyenv
[poetry_install_link]: https://laurencedv.org/computing/python#poetry

<!-- doc links -->

[kicad_doc_link]: https://docs.kicad.org/8.0/en/
[kicad_doc_path_link]: https://docs.kicad.org/8.0/en/kicad/kicad.html#paths_configuration
[kicad_doc_libinstall_link]: https://docs.kicad.org/8.0/en/kicad/kicad.html#libraries-configuration
[kibot_doc_link]: https://kibot.readthedocs.io/en/master/

<!-- emails -->

[maintainer_email]: mailto:laurence@realee.tech

<!-- files -->

[contrib_file]: doc/contrib.md
[roadmap_file]: doc/roadmap.md
[release_file]: doc/release.md
[license_file]: ./license

<!-- folder -->

[doc_folder]: doc/
[footprint_folder]: footprint/
[3dmodel_folder]: 3dmodel/
[simmodel_folder]: simmodel/
[symbol_folder]: symbol/
