import re


def check_unique_column(filename, column_index):
    with open(filename, 'r') as file:
        content = file.read()
        rows = re.findall(r'\|.*\|\n(?:\|.*\|\n)+', content)
        column_values = set()
        for row in rows:
            table_rows = row.strip().split('\n')[2:]  # Skip first two rows
            for table_row in table_rows:
                columns = table_row.strip().split('|')
                column_value = columns[column_index].strip()
                if column_value in column_values:
                    print(f'duplicate found: \'{column_value}\'')
                    return False
                column_values.add(column_value)
    return True


filename = './doc/groups.md'
if check_unique_column(filename, 1):
    print("All groups have a unique name")
else:
    print("A group have a non-unique name")
