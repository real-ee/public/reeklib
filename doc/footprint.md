# Footprint creation

To create a valid footprint all those rules should be observed:

- **Designator** should be present:
  - on silkscreen as the magic `REF**` used by KiCAD, centered on top of part's body (must be visible)
  - on fab as a substituted symbol `${REFERENCE}` _centered_ and _within_ the body outline
    - text should use [default](#footprint-defaults) value if possible, but can be reduced to fit inside outline after substitution
- **Outline** should be marked:
  - on silkscreen as much of the body as possible, especially if the corner can be drawn.
  - on fab with the complete and continuous body, if multiple sub-parts are important, they should be drawn too
  - on all layers, lines should be centered on the actual dimension and using [default](#footprint-defaults) values is possible
- **Pads** should be:
  - all present and named according to datasheet
  - include relevant soldermask opening and paste mask
  - use rounded rectangle for rectangular pad or circle for circular ones, oval and rectangle should be use only on exception
  - use [default](#footprint-defaults) when possible
- **Pin 1** should be marked:
  - on silkscreen by a `^` pointing toward the pin
  - on fab by a notch on the part outline
- **Courtyard** should be defined as a continuous rectangle encompassing all the _pcb-area_ used and the relevant tolerances
- **Placement Anchor** should be centered in both axis
  - exception exist for connectors, _pcb-area_ should be centered in that case

## 3D Models
All 3d models should respect those requirements:
  - The minimum 3d model that should be present is the body of the part itself
  - If it is a connector the *mated part(s)* should also be added but **invisible** by default
  - The model(s) should be a [step][stepfile_link] (*AP214*) stored in the [3dmodel][3dmodel_folder] *linked* using the `${REEKILIB}` variable as root
  - All model(s) should be correctly spatially positioned as it would be assembled on a real PCB
  - All model(s) should be colored realistically (using [default color](#3d-models-defaults) when possible)
  - Must include orientation marking
  - All model(s) should be continuous 3d body (no space between sub-section or multi-section assembly)
  - All model(s) should be simpler rather than detailed (the main purpose is mechanical verification and realistic renders, use the appropriate level of details)
    - ex: package with large number of pins may omit them

## Footprint defaults

- **lines**:
  - silkscreen: 0.1mm
  - fab: 0.1mm
  - courtyard: 0.05mm
- **text**:
  - width: 0.8mm
  - height: 0.8mm
  - thickness: 0.15mm
- **pads**:
  - rounded rectangle: 0.05mm radius
  - circular: soldermask expansion 0.05mm

### 3D models defaults
  - typical black plastic body: `#1a1a1a`
  - tinned metal: `#cecece`
  - gold metal: `#ffd93e`
  - pin1 indicator: `#ffffff`

<!-- doc links -->

[stepfile_link]: https://en.wikipedia.org/wiki/ISO_10303-21

<!-- folder -->

[3dmodel_folder]: 3dmodel/
