# ReeKiLib groups structure

> NOTE: When creating a new part, **Always** use a [specific group](#specific-passives) in priority of a [generic group](#generic)

## Mechanical

| Group ID | Name               | Desc                   |
| :------- | :----------------- | :--------------------- |
| _CAS_    | CASes              | Casing or enclosure    |
| _FST_    | FaSTeners          | Any type of fastener   |
| _SHK_    | SHrinK             | Heat-shrink tubing     |
| _SLV_    | SLeeVes            | Cable or wire sleeving |
| _SPC_    | SPaCers / Standoff | Boards spacer          |

## Electro-Mechanical

| Group ID | Name                | Desc                                            |
| :------- | :------------------ | :---------------------------------------------- |
| _CBL_    | CaBLes              | Cables (assemblies of wires)                    |
| _CON_    | CONnectors          | Generic & specific connectors                   |
| _CTC_    | ConTaCts            | Contact or terminals used in connector or alone |
| _FAN_    | FANs                | Fans                                            |
| _MIC_    | MICrophones         | Any kind of acoustic microphone                 |
| _MTR_    | MoToRs              | Any kind of electric motor                      |
| _MSW_    | Mechanic SWitches   | Mechanically controlled switches (all type)     |
| _PLT_    | PeLTiers            | Thermo-electric peltier device                  |
| _POT_    | POTentiometers      | Potentiometer and other variable resistor       |
| _RSW_    | Rotary SWitches     | Mechanically controlled many:1 switches         |
| _SHD_    | SHielDs             | Electro-magnetic shielding device               |
| _SPK_    | SPeaKers            | Any kind of acoustic speaker                    |
| _VCP_    | Variable CaPacitors | Mechanically variable passive capacitor         |
| _VIN_    | Variable INductors  | Mechanically variable passive inductor          |
| _WIR_    | WIRes               | Individual wires                                |

## Electronic

### Generic

| Group ID | Name                | Desc                                              |
| :------- | :------------------ | :------------------------------------------------ |
| _AIC_    | Analog ICs          | Mostly analog integrated circuit                  |
| _BAT_    | BATteries           | Batteries and long-term energy storage            |
| _DIC_    | Digital ICs         | Digital integrated circuit                        |
| _DIO_    | DIOdes              | Generic diode components                          |
| _GIC_    | Generic ICs         | Most generic integrated circuit                   |
| _LOG_    | LOGic ics           | Boolean logic integrated circuit                  |
| _MEM_    | MEMory ics          | Memory and information storage integrated circuit |
| _PSV_    | PaSsiVe             | Passive electronic components                     |
| _PIC_    | Power ICs           | Power management integrated circuit               |
| _RIF_    | Radio-Frequency ICs | Radio-frequency integrated circuit                |
| _SNS_    | SeNSors             | Sensing and measuring components                  |
| _TRN_    | TRaNsistor          | Generic transistor components                     |

---

### Specific Passives

| Group ID | Name                             | Desc                                                |
| :------- | :------------------------------- | :-------------------------------------------------- |
| _ANT_    | ANTennas                         | Antennas (may contain active devices)               |
| _BLN_    | BaLuNs                           | Passive balanced-to-unbalanced devices              |
| _CAP_    | CAPacitors                       | Passive capacitors                                  |
| _CHO_    | CHOkes                           | Specific 1:1 transformer for common-mode            |
| _IND_    | INDuctors                        | Passive inductors                                   |
| _FER_    | FERrites                         | Ferrite beads or core                               |
| _FLT_    | FiLTers                          | Passive filters                                     |
| _FUS_    | FUSes                            | Single blow fuses                                   |
| _NTC_    | Negative Temperature Coefficient | Thermistor and negative temp. feedback devices      |
| _PTC_    | Positive Temperature Coefficient | Resettable fuse and positive temp. feedback devices |
| _RES_    | RESistors                        | Passive resistors                                   |
| _RSN_    | ReSoNators                       | Passive resonance devices                           |
| _THC_    | THermoCouples                    | Thermocouple junction                               |
| _TRF_    | TRansFormers                     | Transformer (power or signal)                       |

### Specific Integrated Circuits

| Group ID | Name                           | Desc                                               |
| :------- | :----------------------------- | :------------------------------------------------- |
| _ADC_    | Analog to Digital Converters   | Dedicated Analog to Digital Converter              |
| _AMP_    | AMPlifier                      | Integrated amplifier (operational, rf, power, etc) |
| _ASW_    | Analog SWitches                | Electronically controlled analog signal switches   |
| _CDC_    | CoDeCs                         | Converter+Deconverter aka ADC+DAC devices          |
| _CPU_    | Central Processing Units       | Generic processing computing devices               |
| _DAC_    | Digital to Analog Converters   | Dedicated Digital to Analog Converter              |
| _DCP_    | Digital CaPacitors             | Electronically controlled variable capacitors      |
| _DIN_    | Digital INductors              | Electronically controlled variable inductors       |
| _DPT_    | Digital Potentiometers         | Electronically controlled variable resistors       |
| _DSP_    | Digital Signal Processors      | Signal processing specific computing devices       |
| _DSW_    | Data SWitches                  | Electronically controlled digital signal switches  |
| _IOX_    | IO eXpanders                   | Input/Output controller remotely accessible        |
| _ISO_    | signal ISOlators               | Digital signal isolation bridge (any types)        |
| _ISR_    | ISolated Regulators            | Isolated power converter devices                   |
| _LDO_    | Low-Drop Out regulators        | Linear regulators with low vdrop                   |
| _LNR_    | LiNear Regulators              | Linear regulators                                  |
| _MCU_    | Micro-Controller Units         | Micro-controllers                                  |
| _MDM_    | MoDeM                          | Modulator/Demodulator devices                      |
| _MUX_    | MUltipleXers/Demultiplexers    | Electronically controlled 1:many/many:1 switches   |
| _OSC_    | OSCillators                    | Self-resonating devices                            |
| _PSW_    | Power SWitches                 | Electronically controlled Power switches           |
| _REF_    | REFerence voltage/current/etc  | References devices (any types)                     |
| _SPV_    | SuPerVisor                     | Supervision devices (any types)                    |
| _SWR_    | Switching regulators           | Switching regulator and controller devices         |
| _TRX_    | Transceiver                    | Transmitter and Receiver devices                   |
| _VCO_    | Voltage Controlled Oscillators | Electronically controlled oscillator               |

### Specific Semiconductors

| Group ID | Name                                 | Desc |
| :------- | :----------------------------------- | :--- |
| _BJT_    | Bi-Junction Transistors              |      |
| _DOB_    | DiOde Bridges                        |      |
| _DOZ_    | DiOde Zeners                         |      |
| _IGB_    | Insulated-Gate Bipolar transistors   |      |
| _JFE_    | Junction Field-Effect transistors    |      |
| _LED_    | Light-Emitting Diodes                |      |
| _MFE_    | Metal Oxyde Field-Effect transistors |      |
| _THY_    | THYristors                           |      |
| _TVS_    | Transient Voltage Suppression        |      |

<!-- link -->
