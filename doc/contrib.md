# Contribution guideline

> TODO: add instructions to create footprint
> TODO: instructions symbol
> TODO: instructions 3d model
> TODO: instructions simulation model
> TODO: instructions template

Contribution can be made following the principles presented here

## Adding parts

### Naming

Part's name should follow [IPC-7351B][ipc7351b_link] naming convention or its principle

- Parts added should be complete and ready to use, thus must include:

  - a symbol, see [Symbol][contrib-symbol_file] section for details
  - one or multiple footprint, see [Footprint][contrib-footprint_file] section for details
  - a (or multiple) [linked][contrib-footprint_file#3d_models] 3d model(s) in the [3d model folder][3dmodel_folder]
  - Every element (symbols, footprints, etc) should respect its respective specifications

### Adding templates

Templates (projects or others) should be added to [reekicad][reekicad_link]. This is intended to be a parts library



<!-- links -->
[reekicad_link]: https://gitlab.com/real-ee/public/reekicad

<!-- doc links -->

[kicad_doc_template_link]: https://docs.kicad.org/8.0/en/kicad/kicad.html#project-templates
[kicad_doc_textdeco_link]: https://docs.kicad.org/8.0/en/eeschema/eeschema.html#text-comments
[kicad_doc_sim_link]: https://docs.kicad.org/8.0/en/eeschema/eeschema.html#simulator

[ipc7351b_link]: http://ohm.bu.edu/~pbohn/__Engineering_Reference/pcb_layout/pcbmatrix/IPC-7x51%20&%20PCBM%20Land%20Pattern%20Naming%20Convention.pdf

<!-- files -->

[contrib-symbol_file]: symbol.md
[contrib-footprint_file]: footprint.md
[contrib-template_file]: template.md

<!-- folders -->

[3dmodel_folder]: ../3dmodel/
