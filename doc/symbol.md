# Symbol creation

Schematic should be as readable and understandable as possible, thus symbols should follow the following guidelines:

- **Name** should:
  - be as much generic as possible (ie: ATSAMV70Qxxx-C not ATSAMV70Q19A-C)
  - follow the current naming scheme for similar parts
  - multi-part symbol should follow the pattern of the single part and adding a suffix `_2` for dual, `_4` for quad, etc
- **Footprint** should:
  - be correctly associated to the default (most common one) present in [footprint folder][footprint_folder] using the `${REEKILIB}` variable as root
  - be added to *footprint filters* if multiple package are available with the same pinout and pin naming
- **Drawing** should:
  - be representative of the part
    - use standard representation if available
    - use generic rectangular box for complex device
  - should be horizontally biased (no top and bot pins, unless typical symbol calls for it)
  - be as readable as possible, account for pin names and other specificities
  - be centered on both axis (mind the pin alignment to the grid, slight vertical offset is acceptable)
  - include useful and static information (schematic clarity is paramount, i2c addresses are ok, voltage ranges less so)
- **Pins** should:
  - all be of the [default length](#symbol-defaults) or at least long enough for the widest pin designator and align to the _1.27mm grid_
  - have the appropriate _electrical type_ (follow datasheet information)
  - have the appropriate _graphical style_
  - include alternative function (follow datasheet) with their respective _electrical type_ and _graphical style_
  - be _organized_ for typical schematic usage (think of the typical schematic around the part)
    - ground(s) should be placed toward bottom
  - be _named_ according to datasheet using available [decorators][kicad_doc_textdeco_link]
  - be _present_ only if an electrical connection may be needed (no NC)
  - include _central pads_ and other atypical connection point
    - if a specific net is supposed to be connected append to the name in `()`
  - be _grouped_ if multiple pin must be connected to same net
    - exception can be made for specific schematic requirements (ie: dedicated pin for specific decoupling scheme)
- **Fields** should:
  - have a generic but [standard][refdes_link] **reference** designator _[visible]_
  - set a useful **value**, to differentiate the parts or include specifics (ie: resistance for a resistor) _[visible]_
  - include the correct or typical (if multiple) **footprint** _[not visible]_
  - online permalink the _datasheet_ from the manufacturer as **datasheet** (watchout for metatag in url) _[not visible]_ 
  - online permalink the _top-level_ specific product page from the manufacturer  as **link** (watchout for metatag in url) _[not visible]_
  - include at least one valid _orderable part-number_ as **MPN** (hopefully in supply -_-) _[not visible]\_
    - additional valid (same footprint) _MPNs_ should be added as **MPN2**, **MPN3**, etc.
  - include at least the matching *manufacturer* of the main *orderable part-number* as **MFG** _[not visible]_
    - additional *orderable part-number* require their respective manufacturer as **MFG2**, **MFG3**, etc.
- **Multi-part** should:
  - be used if multiple similar elements are present (ie: dual op-amp should use 2)
  - be minimized for "too large to fit" parts and sectioned in a relevant manner (io banks, power pins, etc)

## Symbol defaults
- pin length: 3.81mm

<!-- doc links -->

[kicad_doc_textdeco_link]: https://docs.kicad.org/8.0/en/eeschema/eeschema.html#text-comments
[refdes_link]: https://en.wikipedia.org/wiki/Reference_designator

<!-- folder -->

[footprint_folder]: footprint/
